//console.log("Hello World!")




//fetch using map
fetch('https://jsonplaceholder.typicode.com/todos')

.then((response) => response.json())
.then((json) => {
	const title = json.map(data => data.title);
	
	console.log(title);
});


// fetch request using GET method single to do list item
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'GET'
	})
.then((response) => response.json())
.then((json) => console.log(json));





//fetch using POST
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		title: "Created To Do List Item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


//fetch using PUT
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
    	title: "Update to do list item",
    	status: "Pending",
    	description: "To update my to do list with a different data structure",
    	dateCompleted: "Pending"
	})
})

.then((response) => response.json())
.then((json) => console.log(json));

// fetch request using PATCH method

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"completed": true,
		
    	"title": "Update to do list item's status",
    	"status": "Complete",
    	"dateCompleted": "07/09/21"
	})
})


.then((response) => response.json())
.then((json) => console.log(json));


//Delete method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
.then((response) => response.json())
.then((json) => console.log(json));